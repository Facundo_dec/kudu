angular.module('starter.controllers', [])

  .controller('AppCtrl', function($scope, $ionicModal, $timeout, $http, $ionicLoading, $location, appConfig) {

    $scope.userInfo = {};

    // Form data for the login modal
    $scope.loginData = {};

    // Form data for the regsiter modal
    $scope.registerData = {};

    // Form data for the forgot modal
    $scope.forgotData = {};

    // Form data for the search modal
    $scope.searchData = {};
    
  })
  // Categories
  .controller('CategoriesCtrl', function($scope, $state, $ionicLoading, $http, appConfig) {
    $scope.categoryData = [];
    $scope.openCategory = function($categoryId) {
      angular.forEach($scope.categoryData, function(category, key) {
        if (category.id === $categoryId) {
          window.localStorage.setItem("categoryName", category.name);
          $state.go('app.category', {
            categoryId: $categoryId
          });
          return true;
        }
      });
    }
    $scope.getCategoryListData = function() {
      $ionicLoading.show({
        template: 'Loading...'
      });
      $http({
          method: 'GET',
          url: appConfig.apiCustomUrl + '/categories.json',
        })
        .success(function(response) {
          $ionicLoading.hide();
          // handle success things
          if (response.api_status != 400) {
            $scope.categoryData = response.content.all_category;
          } else {

          }
        });
    };
    $scope.$on("$ionicView.beforeEnter", function(event, data) {
      // handle event
      $scope.getCategoryListData();
    });
  })

  .controller('SingleCategoryCtrl', function($scope, $timeout, $stateParams, $state, $http, $ionicLoading, appConfig) {
    $scope.pageCategory = 50;
    $scope.categoryData = [];
    $scope.canLoadMoreCategoryData = true;
    $timeout(function() {
      $scope.categoryName = window.localStorage.getItem("categoryName");
    }, 700);
    $scope.openVideo = function($videoId) {
      angular.forEach($scope.categoryData, function(video, key) {
        if (video.id === $videoId) {
          window.localStorage.setItem("video", JSON.stringify(video));
          $state.go('app.video', {
            videoId: $videoId
          });
          return true;
        }
      });
    };
    $scope.getCategoryData = function($page) {
      $ionicLoading.show({
        template: 'Loading...'
      });
      $http({
          method: 'GET',
          url: appConfig.apiUrl + '?type=get_videos_by_category&category_id=' + $stateParams.categoryId + '&limit=10&offset=' + $page ,
        })
        .success(function(response) {
          $ionicLoading.hide();
          // handle success things
          if (response.api_status != 400) {
            if (response.data.length > 0) {
              angular.forEach(response.data, function(video, key) {
                $scope.categoryData.push(video);
              });
              $scope.pageCategory = $page - 6 ;
              $scope.$broadcast('scroll.infiniteScrollComplete');
              $scope.$broadcast('scroll.refreshComplete');
            } else {
              $scope.canLoadMoreCategoryData = false;
            }
          } else {

          }
        });
    };
    $scope.loadMoreCategoryData = function() {
      $scope.getCategoryData($scope.pageCategory);
    };
    $scope.refreshCategoryData = function() {
      $scope.pageCategory = 1;
      $scope.categoryData = [];
      $scope.canLoadMoreCategoryData = true;
      $scope.getCategoryData(1);
    };
  })

  .controller('SearchCtrl', function($scope, $state, $http, $ionicLoading, appConfig) {
    $scope.pageSearch = 1;
    $scope.searchData = [];
    $scope.searchFormData = {};
    $scope.canLoadMoreSearchData = false;
    $scope.openVideo = function($videoId) {
      angular.forEach($scope.searchData, function(video, key) {
        if (video.id === $videoId) {
          window.localStorage.setItem("video", JSON.stringify(video));
          $state.go('app.video', {
            videoId: $videoId
          });
          return true;
        }
      });
    };
    $scope.getSearchData = function($page) {
      $ionicLoading.show({
        template: 'Loading...'
      });
      $http({
          method: 'POST',
          url: appConfig.apiUrl + '?type=search_videos&keyword=' + $scope.searchFormData.keyword + '&limit=100',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          transformRequest: function(obj) {
            var str = [];
            for (var p in obj)
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            return str.join("&");
          },
          data: {
            "tags": $scope.searchFormData.tags,
            "limit": 100
          }
        })

        .success(function(response) {
          $ionicLoading.hide();
          // handle success things
          if (response.api_status != 400) {
            if (response.data.length > 0) {
              angular.forEach(response.data, function(video, key) {
                $scope.searchData.push(video);
              });
              $scope.$broadcast('scroll.refreshComplete');
            } else {
              $scope.canLoadMoreSearchData = false;  
              $ionicLoading.show({
                template: 'Sorry. No results, please try another search...',
                 duration: 2100
              });
            }
          } else {

          }
        });
    };

    $scope.doSearch = function() {
      $scope.searchData = [];
      $scope.canLoadMoreSearchData = false;
      $scope.getSearchData(1);
    };
  })

  .controller('VideoCtrl', function($scope, $sce, $http, appConfig) {
    $scope.videoData = JSON.parse(window.localStorage.getItem("video"));
    $scope.video_url = "";
    $scope.userInfo = {};

    $scope.getVideoType = function() {
      return $scope.videoData.source;
    };
    $scope.getVideoUrl = function() {
      if ($scope.videoData.source == "YouTube") {
        if($scope.videoData.video_location.split("v=")[1]){
          $scope.video_url = $sce.trustAsResourceUrl("https://www.youtube.com/embed/"+$scope.videoData.video_location.split("v=")[1].substring(0, 11) + '?showinfo=0');
          return true;
        }
        if($scope.videoData.video_location.split("/")[3]){
          $scope.video_url = $sce.trustAsResourceUrl("https://www.youtube.com/embed/"+$scope.videoData.video_location.split("v=")[4] + '?showinfo=0');
          return true;
        }
      }
      if ($scope.videoData.source == "Vimeo") {
        $scope.video_url = $sce.trustAsResourceUrl($scope.videoData.video_location + '?title=0&byline=0');
        return true;
      }
      if ($scope.videoData.source == "Uploaded") {
        $scope.video_url = $sce.trustAsResourceUrl($scope.videoData.video_location);
        return true;
      }
      $scope.video_url = $sce.trustAsResourceUrl($scope.videoData.video_location);

    };
    $scope.$on("$ionicView.beforeEnter", function(event, data) {
      // handle event
      $scope.getVideoUrl();
      if (window.localStorage.getItem("is_login") !== null && window.localStorage.getItem("is_login") === 'true') {
        $scope.userInfo = JSON.parse(window.localStorage.getItem("userInfo"));
      } else {
        $scope.userInfo.user_id = -1;
      }
    });
  })

  .controller('HomeCtrl', function($scope, $ionicLoading, $http, $state, appConfig) {
    $scope.pageLatestView = 90;
    $scope.canLoadMoreLatestData = true;
    $scope.latestViewData = [];
    $scope.pageMostView = 90;
    $scope.canLoadMoreMostViewData = true;
    $scope.mostViewData = [];
    $scope.openVideo = function($videoId) {
      angular.forEach($scope.latestViewData, function(video, key) {
        if (video.id === $videoId) {
          window.localStorage.setItem("video", JSON.stringify(video));
          $state.go('app.video', {
            videoId: $videoId
          });
          return true;
        }
      });
      angular.forEach($scope.mostViewData, function(video, key) {
        if (video.id === $videoId) {
          window.localStorage.setItem("video", JSON.stringify(video));
          $state.go('app.video', {
            videoId: $videoId
          });
          return true;
        }
      });
    };
    $scope.getLatestViewData = function($page) {
      $ionicLoading.show({
        template: 'Loading...'
      });
      $http({
          method: 'GET',
          url: appConfig.apiUrl + '?type=get_videos&limit=5' + '&latest_offset=' + $page ,
        })
        .success(function(response) {
          $ionicLoading.hide();
          // handle success things
          if (response.api_status != 400) {
            if (response.data.latest.length > 0) {
              angular.forEach(response.data.latest, function(video, key) {
                $scope.latestViewData.push(video);
              });
              $scope.pageLatestView = $page - 6 ;
              $scope.$broadcast('scroll.infiniteScrollComplete');
              $scope.$broadcast('scroll.refreshComplete');
            } else {
              $scope.canLoadMoreLatestData = false;
            }
          } else {

          }
        })
        .error(function(data, status, headers, config) {
          // handle error things
          $ionicLoading.hide();
        });
    };
    $scope.loadMoreLatestViewData = function() {
      $scope.getLatestViewData($scope.pageLatestView);
    };
    $scope.refreshLatestViewData = function() {
      $scope.pageLatestView = 1;
      $scope.canLoadMoreLatestData = true;
      $scope.latestViewData = [];
      $scope.getLatestViewData(1);
    };
    $scope.getMostViewData = function($page) {
      $ionicLoading.show({
        template: 'Loading...'
      });
      $http({
          method: 'GET',
          url: appConfig.apiUrl + '?type=get_videos&limit=30' ,
        })
        .success(function(response) {
          $ionicLoading.hide();
          // handle success things
          if (response.api_status != 400) {
            if (response.data.top.length > 0) {
              angular.forEach(response.data.top, function(video, key) {
                $scope.mostViewData.push(video);
              });
              $scope.canLoadMoreMostViewData = false;
              $scope.$broadcast('scroll.infiniteScrollComplete');
              $scope.$broadcast('scroll.refreshComplete');
            } else {
              $scope.canLoadMoreMostViewData = false;
            }
          } else {

          }
        })
        .error(function(data, status, headers, config) {
          // handle error things
          $ionicLoading.hide();
        });

    };
    $scope.loadMoreMostViewData = function() {
      $scope.getMostViewData($scope.pageMostView);
    };
    $scope.refreshMostViewData = function() {
      $scope.pageMostView = 1;
      $scope.canLoadMoreMostViewData = true;
      $scope.mostViewData = [];
      $scope.getMostViewData(1);
    };
  });
